import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.swt.graphics.RGB;

public class Aplicacion {

	protected Shell shlAplicacion;
	private Text text;
	private LocalResourceManager localResourceManager;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Aplicacion window = new Aplicacion();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlAplicacion.open();
		shlAplicacion.layout();
		while (!shlAplicacion.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlAplicacion = new Shell();
		createResourceManager();
		shlAplicacion.setBackground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 128, 64))));
		shlAplicacion.setSize(450, 300);
		shlAplicacion.setText("Aplicacion");
		
		Label lblHola = new Label(shlAplicacion, SWT.NONE);
		lblHola.setForeground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 255, 255))));
		lblHola.setBackground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 128, 64))));
		lblHola.setBounds(10, 32, 55, 15);
		lblHola.setText("Nombre");
		
		text = new Text(shlAplicacion, SWT.BORDER);
		text.setBackground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(128, 255, 0))));
		text.setBounds(71, 32, 76, 21);

	}
	private void createResourceManager() {
		localResourceManager = new LocalResourceManager(JFaceResources.getResources(),shlAplicacion);
	}
}
